\section{Transformation logarithmique en coordonnées polaires}
\label{log-polar-transformation}
L'avantage de cette transformation par rapport à une transformation en
coordonnées polaires classique est la conservation du rapport d'aspect : il
s'agit d'une transformation \technical{conforme}.

En effet, lorsque l'on passe en coordonnées polaires :
\[
	\mathrm{d}S = \mathrm{d}x \mathrm{d}y = r \mathrm{d}\theta \mathrm{d}r
\]

En utilisant la transformation polaire logarithmique on a :
\[
	\mathrm{d}S = \mathrm{d}x \mathrm{d}y
	            = \mathrm{d}\theta \mathrm{d}\tilde{r}
\]

On considère le plan d'origine en coordonnées complexes. Notons
$z=r + i\theta$ l'affixe d'un point de ce plan. On obtient l'affixe $\tilde{z}$
de ce point dans le plan d'arrivée à l'aide de l'expression :
\[
	\tilde{z} = R \exp \left( \frac{z}{R} \right)
\]
où $R$ est le rayon du cercle dont la longueur du périmetre est inchangée
par la transformation.

\begin{figure}[h]
\centering
\begin{subfigure}{0.4\textwidth}
	\centering
	\begin{tikzpicture}
		\coordinate [label=left:$0$] (0) at (0, 0);
		\coordinate (corner) at (4, -4.25);
		\fill[very-light-blue]
		(0) rectangle (corner);
		\draw[draw=light-blue, thick]
		(0, -4.25)
		-- (0)
		-- (4, 0)
		-- (corner);
		\draw[step=0.25cm,gray,very thin]
		(-0.5, -4.25) grid (4.75, 1);
		\node at (4, 0)
		[cross out,draw,minimum size=5,rotate=45,
		label=below left:$2\pi$] {};
		\draw[-stealth, thick]
		(0) -- (4.5, 0) node[above left]{$\theta$};
		\draw[-stealth, thick]
		(0, -4.25) -- (0, 0.75) node[below left]{$r$};
	\end{tikzpicture}
\end{subfigure}%
\begin{subfigure}{0.2\textwidth}
	\centering
	\begin{tikzpicture}
		\draw[->,very thick] (0,0) -- (2,0);
	\end{tikzpicture}
\end{subfigure}%
\begin{subfigure}{0.4\textwidth}
	\centering
	\begin{tikzpicture}
		\fill[very-light-blue, draw=light-blue, thick]
		(0.125, 0.125) circle [radius=2.5];
		\draw[step=0.25cm,gray,very thin]
		(-2.5, -2.5) grid (2.75, 2.75);
		\coordinate [label=below:$O$] (O) at (0.125, 0.125);
		\coordinate [label=left:$A$] (A) at (0.125, 2.625);
		\coordinate [label=right:$B'$] (B) at ({90-0.1r}:2.5);
		\fill[orange]
		(O)
		-- (0.125, 1.3125)
		arc (90:{90-0.1r}:1.25)
		-- cycle;
		\draw
		(A)
		arc (90:{90-0.1r}:2.5)
		-- (O)
		-- cycle;
		\node[dark-orange] at (0.5, 0.984375) {$\Delta\theta$};
		\node at (-0.125, 1.375) {$R$};
	\end{tikzpicture}
\end{subfigure}
\end{figure}

\subsection{Discrétisation angulaire}
Pour ne pas perdre d'information par discrétisation, on choisit
$k_\theta \in \mathbb{N}$ la discrétisation angulaire telle que le plus petit
angle entre deux pixels adjacents et l'origine du repère $\Delta\theta$
vérifie $k_\theta \Delta\theta = \theta$ dans le plan de départ.

Considérons le triangle $ABO$, où $B$ est le centre du pixel sur lequel $B'$
se trouve.\\
Notons alors $a = BO$, $b = AO$, $c = AB$ et $\alpha, \beta, \gamma$ les
angles opposés à $a, b, c$ respectivement. Les longueurs $b$ et $c$ sont
connues, ainsi que $\alpha = \frac{\pi}{2}$. Toutes les longueurs et tous les
angles sont positifs.

On a la relation $R = \frac{1}{\gamma}$, donc il suffit d'exprimer
$\gamma = \Delta\theta$.
Comme $\alpha = \frac{\pi}{2}$, on a
\[
	\tan(\gamma) = \frac{c}{b}
	\iff \gamma \equiv \arctan \left( \frac{c}{b} \right) \pmod{\pi}
\]
Si on restreint $\gamma$ à l'intervalle $\left] 0, \frac{\pi}{2} \right[$,
on obtient
\[
	\gamma = \arctan \left( \frac{c}{b} \right) \implies
	R = \frac{1}{\arctan \left( \frac{c}{b} \right)}
\]

On peut maintenant déterminer la discrétisation angulaire la plus fine :
\[
	\theta \in \left\{ k_{\theta} \gamma,
	k_{\theta} \in \llbracket 0,
	               \lfloor 2 \pi R \rfloor \rrbracket \right\}.
\]

\subsection{Discrétisation radiale}
Pour ne pas perdre d'information par discrétisation, on choisit
$k_r \in \mathbb{Z}$ la discrétisation radiale telle que
$\Delta r=1\mathrm{px}$ dans le repère d'origine corresponde à 1\textrm{px}
de la couronne extérieure, après transformation.

Soient $P$ et $Q$ deux points de coordonnées polaires respectives $(R, \theta)$
et $(R-1, \theta)$ dans l'espace d'arrivée, et de coordonnées $r_P+i\theta$ et
$r_Q+i\theta$ dans l'espace de départ, avec $r_P = r_Q + 1$. $\theta$ est
quelconque.
\begin{align*}
	\Delta r
	&= 1\\
	&= R - (R-1)\\
	&= |P| - |Q|\\
	&= |P| - R \exp \left( \frac{r_Q}{R} \right)\\
	&= |P| - R \exp \left( \frac{r_P - 1}{R} \right)\\
	&= |P| - R \exp \left( \frac{r_P}{R} \right)
	           \exp \left( - \frac{1}{R} \right)\\
	&= |P| - |P| \exp \left( - \frac{1}{R} \right)\\
	&= R - R \exp \left( - \frac{1}{R} \right)\\
	&= R \left( 1 - \exp \left( - \frac{1}{R} \right) \right)\\
\end{align*}

Déterminons maintenant les bornes de $r$.\\
La borne maximale est évidemment définie par
$R = R \exp \left( \frac{r_\text{max}}{R} \right) \iff r_\text{max} = 0$.\\
Imposer une borne minimale finie correspond à retirer un disque centré en $O$.
Si on choisit de retirer un disque de rayon $C$, alors on cherche
$r_\text{min}$ tel que $C = R \exp \left( \frac{r_\text{min}}{R} \right) \iff
r_\text{min} = R \ln \frac{C}{R}$. En posant $C=1\mathrm{px}$, on obtient
$r_\text{min} = - R \ln R$.

On peut maintenant déterminer la discrétisation radiale la plus fine :
\[
	r \in \left\{ k_r R \left(
	1 - \exp \left( - \frac{1}{R} \right)
	\right),
	k_R \in \llbracket \lfloor - R \ln R \rfloor,0 \rrbracket \right\}
\]

