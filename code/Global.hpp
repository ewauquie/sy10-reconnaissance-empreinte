#ifndef GLOBAL_HPP
#define GLOBAL_HPP

/* Ce fichier définit des constantes de préprocesseur
 * utilisées tout au long de l'exécution du programme
 */

    #define SAVE_MINUTIA_IMAGES // Sauvegarde les images overlays des minuties
    //#define SAVE_SCANNING_IMAGES // Sauvegarde les images utilisées par les FIS au format "section-<resolution>-<r>_<theta>.png"
    //#define SAVE_SCANNING_IMAGES_MINUTIAS // Sauvegarde les images de minuties reconnues par un FIS au format "<minutie>-<resolution>-<r>_<theta>.png"
    //#define SAVE_TRANSFORMED_IMAGE // Sauvegarde l'image transformée en coordonnées polaires logarithmiques

    #define DB_PATH ".fingerprint.db"
    #define DB_VERSION "1"

    #define COMPARE_COUNT 20 // Nombre de minuties à comparer
    #define COMPARE_RADIUS 2 // Rayon de proximité en sillons

    #define CENTER_RADIUS_IGNORE 1 // Rayon du disque central en sillons. Ce disque est ignoré lors de l'analyse
    #define STEP_PER_RIDGE 6 // Nombre d'analyse (appel aux FIS) par longueur de sillon
    #define PIXEL_SIZE 0.2 // Nombre de sillon par pixel
    #define RIDGE_COUNT 60 // Diamètre de l'empreinte analysé (en sillons)

    #define INSPECT_ANALYSIS

  #ifdef INSPECT_ANALYSIS
    #define PREVIEW_SIZE 25 // Taille de chaque côté du curseur
    #define ANALYZED_IMAGE_DEFAULT_RES 7
  #endif

#endif // GLOBAL_HPP
