#include "MinutiaEngine.hpp"

MinutiaEngine::MinutiaEngine()
    : fl::Engine() {

    fl::OutputVariable *value = new fl::OutputVariable;
    value->setName("<value>");
    value->setEnabled(true);
    value->setRange(0, 1);
    value->setLockValueInRange(true);
    value->setAggregation(new fl::Maximum);
    value->setDefuzzifier(new fl::WeightedAverage);
    value->setDefaultValue(fl::nan);
    value->setLockPreviousValue(false);
    value->addTerm(new fl::Ramp("'value'", 0, 1));
    addOutputVariable(value);
}

MinutiaEngine::MinutiaEngine(int dataSize)
    : fl::Engine() {

    for (int x(0); x < dataSize; ++x) {
        for (int y(0); y < dataSize; ++y) {
            fl::InputVariable *pixel = new fl::InputVariable;
            pixel->setName("<pixel_" + QString::number(x).toStdString() + "x" + QString::number(y).toStdString() + ">");
            pixel->setEnabled(true);
            pixel->setRange(0, 1);
            pixel->setLockValueInRange(true);
            pixel->addTerm(new fl::Ramp("'white'", 0.6, 0.9));
            pixel->addTerm(new fl::Ramp("'black'", 0.4, 0.1));
            pixel->addTerm(new fl::Trapezoid("'gray'", 0.25, 0.4, 0.6, 0.75));
            addInputVariable(pixel);
        }
    }

    fl::OutputVariable *value = new fl::OutputVariable;
    value->setName("<value>");
    value->setEnabled(true);
    value->setRange(0, 1);
    value->setLockValueInRange(true);
    value->setAggregation(new fl::Maximum);
    value->setDefuzzifier(new fl::WeightedAverage);
    value->setDefaultValue(fl::nan);
    value->setLockPreviousValue(false);
    value->addTerm(new fl::Ramp("'value'", 0, 1));
    addOutputVariable(value);
}

void MinutiaEngine::processImage(const QImage *src) {
    int dataSize = this->dataSize();
    if (src->width() == dataSize && src->height() == dataSize) {
        // Assigne les valeurs à chaque `fl::InputVariable`
        for (int i(0); i < dataSize * dataSize; ++i) {
            int x = qFloor(i / dataSize);
            int y = i % dataSize;
            getInputVariable("<pixel_" + QString::number(x).toStdString() + "x" + QString::number(y).toStdString() + ">")
                    ->setValue(src->pixelColor(QPoint(x, y)).lightnessF());
        }
    }
}

qreal MinutiaEngine::value() const {
    return std::stod(getOutputVariable("<value>")->fuzzyOutputValue().substr(0, 5));;
}

void MinutiaEngine::extendRule(std::string rule) {
    /* Ajoute 7 nouvelles règles à partir de `rule` par symétries et rotation
     *
     */
    std::string rot1(rotRule(rule));
    std::string rot2(rotRule(rot1));
    std::string rot3(rotRule(rot2));
    std::string mirror0(mirrorRule(rule));
    std::string mirror1(rotRule(mirror0));
    std::string mirror2(rotRule(mirror1));
    std::string mirror3(rotRule(mirror2));

    fl::RuleBlock* ruleBlock = getRuleBlock(0);
    ruleBlock->addRule(fl::Rule::parse(rot1, this));
    ruleBlock->addRule(fl::Rule::parse(rot2, this));
    ruleBlock->addRule(fl::Rule::parse(rot3, this));
    ruleBlock->addRule(fl::Rule::parse(mirror0, this));
    ruleBlock->addRule(fl::Rule::parse(mirror1, this));
    ruleBlock->addRule(fl::Rule::parse(mirror2, this));
    ruleBlock->addRule(fl::Rule::parse(mirror3, this));
}

std::string MinutiaEngine::rotRule(std::string rule) const {
    int dataSize = this->dataSize();
    if (dataSize % 2 == 1) {
        std::string newRule;
        unsigned long lastPos(0);
        for (unsigned long i(0); i < rule.size(); ++i) {
            if (rule.at(i) == 'x') {
                std::string x(rule.substr(i - 1, 1));
                std::string y(rule.substr(i + 1, 1));
                if (std::isdigit(x.front()) && std::isdigit(y.back())) {
                    while (std::isdigit(x.front())) {
                        x.insert(0, 1, rule.at(i - x.size() - 1));
                    }
                    x.erase(0, 1);
                    while (std::isdigit(y.back())) {
                        y.append(rule.substr(i + y.size() + 1, 1));
                    }
                    y.pop_back();

                    int X = std::stoi(x) - dataSize / 2;
                    int Y = std::stoi(y) - dataSize / 2;
                    int newX = -Y + dataSize / 2;
                    int newY = X + dataSize / 2;

                    newRule.append(rule.substr(lastPos, i - x.size() - lastPos));
                    newRule.append(std::to_string(newX));
                    newRule.append("x");
                    newRule.append(std::to_string(newY));

                    lastPos = i + y.size() + 1;
                }
            }
        }
        newRule.append(rule.substr(lastPos));
        return newRule;
    }
    return std::string();
}

std::string MinutiaEngine::mirrorRule(std::string rule) const {
    int dataSize = this->dataSize();
    if (dataSize % 2 == 1) {
        std::string newRule;
        unsigned long lastPos(0);
        for (unsigned long i(0); i < rule.size(); ++i) {
            if (rule.at(i) == 'x') {
                std::string x(rule.substr(i - 1, 1));
                std::string y(rule.substr(i + 1, 1));
                if (std::isdigit(x.front()) && std::isdigit(y.back())) {
                    while (std::isdigit(x.front())) {
                        x.insert(0, 1, rule.at(i - x.size() - 1));
                    }
                    x.erase(0, 1);
                    while (std::isdigit(y.back())) {
                        y.append(rule.substr(i + y.size() + 1, 1));
                    }
                    y.pop_back();

                    int X = std::stoi(x) - dataSize / 2;
                    int Y = std::stoi(y) - dataSize / 2;
                    int newX = -X + dataSize / 2;
                    int newY = -Y + dataSize / 2;

                    newRule.append(rule.substr(lastPos, i - x.size() - lastPos));
                    newRule.append(std::to_string(newX));
                    newRule.append("x");
                    newRule.append(std::to_string(newY));

                    lastPos = i + y.size() + 1;
                }
            }
        }
        newRule.append(rule.substr(lastPos));
        return newRule;
    }
    return std::string();
}
