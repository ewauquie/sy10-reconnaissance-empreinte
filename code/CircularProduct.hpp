#ifndef FL_CIRCULAR_PRODUCT_HPP
#define FL_CIRCULAR_PRODUCT_HPP

#include "fuzzylite/fl/norm/TNorm.h"

#include <math.h>

namespace fl {
    class FL_API CircularProduct FL_IFINAL : public TNorm {
    public:
        std::string className() const FL_IOVERRIDE;

        Complexity complexity() const FL_IOVERRIDE;
        scalar compute(scalar a, scalar b) const FL_IOVERRIDE;
        CircularProduct* clone() const FL_IOVERRIDE;

        static TNorm* constructor();
    };
}
#endif  /* FL_CIRCULAR_PRODUCT_HPP */
