#include "ReverseTrapezoid.hpp"

namespace fl {
    ReverseTrapezoid::ReverseTrapezoid(const std::string& name,
                                       scalar vertexA, scalar vertexB, scalar vertexC, scalar vertexD, scalar height)
    : Term(name, height), _vertexA(vertexA), _vertexB(vertexB), _vertexC(vertexC), _vertexD(vertexD) {
        if (Op::isNaN(vertexC) and Op::isNaN(vertexD)) {
            this->_vertexD = _vertexB;
            scalar range = _vertexD - _vertexA;
            this->_vertexB = _vertexA + range * 1.0 / 5.0;
            this->_vertexC = _vertexA + range * 4.0 / 5.0;
        }
    }

    ReverseTrapezoid::~ReverseTrapezoid() {}

    std::string ReverseTrapezoid::className() const {
        return "ReverseTrapezoid";
    }

    Complexity ReverseTrapezoid::complexity() const {
        return Complexity().comparison(1 + 6).arithmetic(1 + 3).function(1);
    }

    scalar ReverseTrapezoid::membership(scalar x) const {
        return getHeight() - Trapezoid(getName(), _vertexA, _vertexB, _vertexC, _vertexD, getHeight()).membership(x);
    }

    std::string ReverseTrapezoid::parameters() const {
        return Op::join(4, " ", _vertexA, _vertexB, _vertexC, _vertexD)+
                (not Op::isEq(getHeight(), 1.0) ? " " + Op::str(getHeight()) : "");
    }

    void ReverseTrapezoid::configure(const std::string& parameters) {
        if (parameters.empty()) return;
        std::vector<std::string> values = Op::split(parameters, " ");
        std::size_t required = 4;
        if (values.size() < required) {
            std::ostringstream ex;
            ex << "[configuration error] term <" << className() << ">"
                    << " requires <" << required << "> parameters";
            throw Exception(ex.str(), FL_AT);
        }
        setVertexA(Op::toScalar(values.at(0)));
        setVertexB(Op::toScalar(values.at(1)));
        setVertexC(Op::toScalar(values.at(2)));
        setVertexD(Op::toScalar(values.at(3)));
        if (values.size() > required)
            setHeight(Op::toScalar(values.at(required)));
    }

    void ReverseTrapezoid::setVertexA(scalar a) {
        this->_vertexA = a;
    }

    scalar ReverseTrapezoid::getVertexA() const {
        return this->_vertexA;
    }

    void ReverseTrapezoid::setVertexB(scalar b) {
        this->_vertexB = b;
    }

    scalar ReverseTrapezoid::getVertexB() const {
        return this->_vertexB;
    }

    void ReverseTrapezoid::setVertexC(scalar c) {
        this->_vertexC = c;
    }

    scalar ReverseTrapezoid::getVertexC() const {
        return this->_vertexC;
    }

    void ReverseTrapezoid::setVertexD(scalar d) {
        this->_vertexD = d;
    }

    scalar ReverseTrapezoid::getVertexD() const {
        return this->_vertexD;
    }

    ReverseTrapezoid* ReverseTrapezoid::clone() const {
        return new ReverseTrapezoid(*this);
    }

    Term* ReverseTrapezoid::constructor() {
        return new ReverseTrapezoid;
    }
}
