#ifndef BIFURCATIONENGINE_H
#define BIFURCATIONENGINE_H

#include <QVector>

#include "fuzzylite/fl/activation/General.h"
#include "fuzzylite/fl/defuzzifier/WeightedAverage.h"
#include "fuzzylite/fl/norm/s/DrasticSum.h"
#include "fuzzylite/fl/norm/s/Maximum.h"
#include "fuzzylite/fl/norm/t/BoundedDifference.h"
#include "fuzzylite/fl/norm/t/Minimum.h"
#include "fuzzylite/fl/rule/Antecedent.h"
#include "fuzzylite/fl/rule/Rule.h"
#include "fuzzylite/fl/rule/RuleBlock.h"
#include "fuzzylite/fl/term/Ramp.h"
#include "fuzzylite/fl/term/Triangle.h"
#include "fuzzylite/fl/variable/InputVariable.h"
#include "fuzzylite/fl/variable/OutputVariable.h"

#include "ReverseTrapezoid.hpp"
#include "MinutiaEngine.hpp"


// FIS détectant les bifurcations

class BifurcationEngine : public MinutiaEngine {
public:
    BifurcationEngine();

    int dataSize() const override;
    MinutiaEngine::Engine engineType() const override;
    void processImage(const QImage *src) override;

protected:
    static constexpr int POINT_COUNT = 12; // Nombre de points du  cercle externe
};

#endif // BIFURCATIONENGINE_H
