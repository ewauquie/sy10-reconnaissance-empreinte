#-------------------------------------------------
#
# Project created by QtCreator 2018-11-19T08:47:31
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = code
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11 thread
QMAKE_CXXFLAGS += -O3

SOURCES += \
    main.cpp \
    Analyzer.cpp \
    BifurcationEngine.cpp \
    MainWindow.cpp \
    MinutiaEngine.cpp \
    Overlay.cpp \
    ReverseTrapezoid.cpp \
    EndingEngine.cpp \
    InspectAnalysisOverlay.cpp \
    CompareEngine.cpp \
    CircularProduct.cpp

HEADERS += \
    Analyzer.hpp \
    BifurcationEngine.hpp \
    MainWindow.hpp \
    MinutiaEngine.hpp \
    Overlay.hpp \
    ReverseTrapezoid.hpp \
    EndingEngine.hpp \
    InspectAnalysisOverlay.hpp \
    Global.hpp \
    CompareEngine.hpp \
    CircularProduct.hpp

FUZZYLITE_HEADERS = \
    fuzzylite/fl/fuzzylite.h \
    fuzzylite/fl/defuzzifier/MeanOfMaximum.h \
    fuzzylite/fl/defuzzifier/IntegralDefuzzifier.h \
    fuzzylite/fl/defuzzifier/WeightedAverageCustom.h \
    fuzzylite/fl/defuzzifier/WeightedDefuzzifier.h \
    fuzzylite/fl/defuzzifier/WeightedAverage.h \
    fuzzylite/fl/defuzzifier/Defuzzifier.h \
    fuzzylite/fl/defuzzifier/WeightedSumCustom.h \
    fuzzylite/fl/defuzzifier/LargestOfMaximum.h \
    fuzzylite/fl/defuzzifier/WeightedSum.h \
    fuzzylite/fl/defuzzifier/SmallestOfMaximum.h \
    fuzzylite/fl/defuzzifier/Centroid.h \
    fuzzylite/fl/defuzzifier/Bisector.h \
    fuzzylite/fl/Benchmark.h \
    fuzzylite/fl/imex/CppExporter.h \
    fuzzylite/fl/imex/Exporter.h \
    fuzzylite/fl/imex/FisExporter.h \
    fuzzylite/fl/imex/FllExporter.h \
    fuzzylite/fl/imex/FclExporter.h \
    fuzzylite/fl/imex/FisImporter.h \
    fuzzylite/fl/imex/Importer.h \
    fuzzylite/fl/imex/FclImporter.h \
    fuzzylite/fl/imex/FldExporter.h \
    fuzzylite/fl/imex/FllImporter.h \
    fuzzylite/fl/imex/JavaExporter.h \
    fuzzylite/fl/imex/RScriptExporter.h \
    fuzzylite/fl/term/Spike.h \
    fuzzylite/fl/term/Term.h \
    fuzzylite/fl/term/Triangle.h \
    fuzzylite/fl/term/Binary.h \
    fuzzylite/fl/term/SigmoidDifference.h \
    fuzzylite/fl/term/Discrete.h \
    fuzzylite/fl/term/GaussianProduct.h \
    fuzzylite/fl/term/Concave.h \
    fuzzylite/fl/term/Sigmoid.h \
    fuzzylite/fl/term/Linear.h \
    fuzzylite/fl/term/SShape.h \
    fuzzylite/fl/term/Cosine.h \
    fuzzylite/fl/term/PiShape.h \
    fuzzylite/fl/term/Rectangle.h \
    fuzzylite/fl/term/Aggregated.h \
    fuzzylite/fl/term/Function.h \
    fuzzylite/fl/term/Ramp.h \
    fuzzylite/fl/term/Constant.h \
    fuzzylite/fl/term/SigmoidProduct.h \
    fuzzylite/fl/term/Trapezoid.h \
    fuzzylite/fl/term/Gaussian.h \
    fuzzylite/fl/term/Activated.h \
    fuzzylite/fl/term/Bell.h \
    fuzzylite/fl/term/ZShape.h \
    fuzzylite/fl/activation/Lowest.h \
    fuzzylite/fl/activation/General.h \
    fuzzylite/fl/activation/Last.h \
    fuzzylite/fl/activation/Activation.h \
    fuzzylite/fl/activation/Highest.h \
    fuzzylite/fl/activation/First.h \
    fuzzylite/fl/activation/Proportional.h \
    fuzzylite/fl/activation/Threshold.h \
    fuzzylite/fl/Console.h \
    fuzzylite/fl/Engine.h \
    fuzzylite/fl/hedge/Seldom.h \
    fuzzylite/fl/hedge/Very.h \
    fuzzylite/fl/hedge/Not.h \
    fuzzylite/fl/hedge/HedgeFunction.h \
    fuzzylite/fl/hedge/Somewhat.h \
    fuzzylite/fl/hedge/Hedge.h \
    fuzzylite/fl/hedge/Extremely.h \
    fuzzylite/fl/hedge/Any.h \
    fuzzylite/fl/variable/InputVariable.h \
    fuzzylite/fl/variable/Variable.h \
    fuzzylite/fl/variable/OutputVariable.h \
    fuzzylite/fl/Operation.h \
    fuzzylite/fl/Headers.h \
    fuzzylite/fl/rule/Expression.h \
    fuzzylite/fl/rule/Rule.h \
    fuzzylite/fl/rule/Consequent.h \
    fuzzylite/fl/rule/Antecedent.h \
    fuzzylite/fl/rule/RuleBlock.h \
    fuzzylite/fl/factory/SNormFactory.h \
    fuzzylite/fl/factory/TermFactory.h \
    fuzzylite/fl/factory/HedgeFactory.h \
    fuzzylite/fl/factory/FunctionFactory.h \
    fuzzylite/fl/factory/FactoryManager.h \
    fuzzylite/fl/factory/ActivationFactory.h \
    fuzzylite/fl/factory/ConstructionFactory.h \
    fuzzylite/fl/factory/CloningFactory.h \
    fuzzylite/fl/factory/DefuzzifierFactory.h \
    fuzzylite/fl/factory/TNormFactory.h \
    fuzzylite/fl/Complexity.h \
    fuzzylite/fl/Exception.h \
    fuzzylite/fl/norm/Norm.h \
    fuzzylite/fl/norm/t/DrasticProduct.h \
    fuzzylite/fl/norm/t/BoundedDifference.h \
    fuzzylite/fl/norm/t/EinsteinProduct.h \
    fuzzylite/fl/norm/t/TNormFunction.h \
    fuzzylite/fl/norm/t/AlgebraicProduct.h \
    fuzzylite/fl/norm/t/Minimum.h \
    fuzzylite/fl/norm/t/HamacherProduct.h \
    fuzzylite/fl/norm/t/NilpotentMinimum.h \
    fuzzylite/fl/norm/SNorm.h \
    fuzzylite/fl/norm/s/NilpotentMaximum.h \
    fuzzylite/fl/norm/s/UnboundedSum.h \
    fuzzylite/fl/norm/s/HamacherSum.h \
    fuzzylite/fl/norm/s/AlgebraicSum.h \
    fuzzylite/fl/norm/s/BoundedSum.h \
    fuzzylite/fl/norm/s/Maximum.h \
    fuzzylite/fl/norm/s/SNormFunction.h \
    fuzzylite/fl/norm/s/NormalizedSum.h \
    fuzzylite/fl/norm/s/EinsteinSum.h \
    fuzzylite/fl/norm/s/DrasticSum.h \
    fuzzylite/fl/norm/TNorm.h

FUZZYLITE_SOURCES = \
    fuzzylite/fuzzylite.cpp \
    fuzzylite/Complexity.cpp \
    fuzzylite/defuzzifier/IntegralDefuzzifier.cpp \
    fuzzylite/defuzzifier/SmallestOfMaximum.cpp \
    fuzzylite/defuzzifier/WeightedSum.cpp \
    fuzzylite/defuzzifier/Centroid.cpp \
    fuzzylite/defuzzifier/LargestOfMaximum.cpp \
    fuzzylite/defuzzifier/WeightedDefuzzifier.cpp \
    fuzzylite/defuzzifier/Bisector.cpp \
    fuzzylite/defuzzifier/WeightedAverage.cpp \
    fuzzylite/defuzzifier/MeanOfMaximum.cpp \
    fuzzylite/defuzzifier/WeightedSumCustom.cpp \
    fuzzylite/defuzzifier/WeightedAverageCustom.cpp \
    fuzzylite/imex/FldExporter.cpp \
    fuzzylite/imex/FclExporter.cpp \
    fuzzylite/imex/FisImporter.cpp \
    fuzzylite/imex/RScriptExporter.cpp \
    fuzzylite/imex/FisExporter.cpp \
    fuzzylite/imex/Exporter.cpp \
    fuzzylite/imex/Importer.cpp \
    fuzzylite/imex/JavaExporter.cpp \
    fuzzylite/imex/FllExporter.cpp \
    fuzzylite/imex/CppExporter.cpp \
    fuzzylite/imex/FllImporter.cpp \
    fuzzylite/imex/FclImporter.cpp \
    fuzzylite/term/Constant.cpp \
    fuzzylite/term/GaussianProduct.cpp \
    fuzzylite/term/ZShape.cpp \
    fuzzylite/term/PiShape.cpp \
    fuzzylite/term/Rectangle.cpp \
    fuzzylite/term/Ramp.cpp \
    fuzzylite/term/Concave.cpp \
    fuzzylite/term/Triangle.cpp \
    fuzzylite/term/Spike.cpp \
    fuzzylite/term/SigmoidProduct.cpp \
    fuzzylite/term/Gaussian.cpp \
    fuzzylite/term/Sigmoid.cpp \
    fuzzylite/term/Discrete.cpp \
    fuzzylite/term/Linear.cpp \
    fuzzylite/term/Binary.cpp \
    fuzzylite/term/Term.cpp \
    fuzzylite/term/Activated.cpp \
    fuzzylite/term/SShape.cpp \
    fuzzylite/term/Trapezoid.cpp \
    fuzzylite/term/Aggregated.cpp \
    fuzzylite/term/SigmoidDifference.cpp \
    fuzzylite/term/Bell.cpp \
    fuzzylite/term/Cosine.cpp \
    fuzzylite/term/Function.cpp \
    fuzzylite/activation/First.cpp \
    fuzzylite/activation/Highest.cpp \
    fuzzylite/activation/Threshold.cpp \
    fuzzylite/activation/General.cpp \
    fuzzylite/activation/Proportional.cpp \
    fuzzylite/activation/Lowest.cpp \
    fuzzylite/activation/Last.cpp \
    fuzzylite/Exception.cpp \
    fuzzylite/hedge/Seldom.cpp \
    fuzzylite/hedge/Somewhat.cpp \
    fuzzylite/hedge/HedgeFunction.cpp \
    fuzzylite/hedge/Very.cpp \
    fuzzylite/hedge/Extremely.cpp \
    fuzzylite/hedge/Any.cpp \
    fuzzylite/hedge/Not.cpp \
    fuzzylite/Engine.cpp \
    fuzzylite/variable/InputVariable.cpp \
    fuzzylite/variable/OutputVariable.cpp \
    fuzzylite/variable/Variable.cpp \
    fuzzylite/rule/RuleBlock.cpp \
    fuzzylite/rule/Expression.cpp \
    fuzzylite/rule/Antecedent.cpp \
    fuzzylite/rule/Consequent.cpp \
    fuzzylite/rule/Rule.cpp \
    fuzzylite/factory/DefuzzifierFactory.cpp \
    fuzzylite/factory/ActivationFactory.cpp \
    fuzzylite/factory/SNormFactory.cpp \
    fuzzylite/factory/HedgeFactory.cpp \
    fuzzylite/factory/FunctionFactory.cpp \
    fuzzylite/factory/FactoryManager.cpp \
    fuzzylite/factory/TermFactory.cpp \
    fuzzylite/factory/TNormFactory.cpp \
    fuzzylite/Benchmark.cpp \
    fuzzylite/norm/t/TNormFunction.cpp \
    fuzzylite/norm/t/Minimum.cpp \
    fuzzylite/norm/t/EinsteinProduct.cpp \
    fuzzylite/norm/t/HamacherProduct.cpp \
    fuzzylite/norm/t/BoundedDifference.cpp \
    fuzzylite/norm/t/AlgebraicProduct.cpp \
    fuzzylite/norm/t/NilpotentMinimum.cpp \
    fuzzylite/norm/t/DrasticProduct.cpp \
    fuzzylite/norm/s/NilpotentMaximum.cpp \
    fuzzylite/norm/s/Maximum.cpp \
    fuzzylite/norm/s/NormalizedSum.cpp \
    fuzzylite/norm/s/HamacherSum.cpp \
    fuzzylite/norm/s/EinsteinSum.cpp \
    fuzzylite/norm/s/DrasticSum.cpp \
    fuzzylite/norm/s/AlgebraicSum.cpp \
    fuzzylite/norm/s/SNormFunction.cpp \
    fuzzylite/norm/s/BoundedSum.cpp \
    fuzzylite/norm/s/UnboundedSum.cpp \
    fuzzylite/Console.cpp

SOURCES += $$FUZZYLITE_SOURCES
HEADERS += $$FUZZYLITE_HEADERS

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
