#include "Overlay.hpp"

Overlay::Overlay(QWidget *parent)
    : QWidget(parent),
      m_isActive(false),
      m_isCenterSet(false),
      m_isMouseIn(false) {
    setAttribute(Qt::WA_NoSystemBackground);
    setAttribute(Qt::WA_TransparentForMouseEvents);
    if (!m_centerPixmap.load("res/center-select-dot.png")) { // TODO: Add a hash check
        QMessageBox::critical(this, tr("Missing file"), tr("Error : file \"res/center-select-dot.png\" is missing."));
    }
    if (!m_fingerprintSymbol.load("res/fingerprint-symbol.png")) { // TODO: Add a hash check
        QMessageBox::critical(this, tr("Missing file"), tr("Error : file \"res/fingerprint-symbol.png\" is missing."));
    }
}

void Overlay::paintEvent(QPaintEvent*) {
    int w = m_centerPixmap.width();
    int h = m_centerPixmap.height();
    QPainter *painter = new QPainter(this);
    if (m_isCenterSet) {
        if (m_isMouseIn) {
            // Dessine le symbole d'empreinte entre le centre et la position du curseur
            const qreal length = qSqrt(qPow(m_centerPosition.x() - m_currentMousePos.x(), 2) +
                                 qPow(m_centerPosition.y() - m_currentMousePos.y(), 2));
            const qreal fingerprintSymbolLength = m_fingerprintSymbol.width() / 2;
            const qreal scalingFactor = length / fingerprintSymbolLength;
            QTransform transform;
            transform.rotate(qRadiansToDegrees(angle()));
            transform.scale(scalingFactor, scalingFactor);
            QPixmap transformedPm = m_fingerprintSymbol.transformed(transform);
            painter->drawPixmap(QRect(m_centerPosition.x() - transformedPm.width() / 2, m_centerPosition.y() - transformedPm.height() / 2,
                                      transformedPm.width(), transformedPm.height()),
                                transformedPm, transformedPm.rect());
        }
        // Dessine le symbole du centre
        painter->drawPixmap(QRectF(static_cast<qreal>(m_centerPosition.x() - w / 2),
                                         static_cast<qreal>(m_centerPosition.y() - h / 2), w, h),
                                  m_centerPixmap, QRectF(0, 0, w, h));
    } else if (m_isMouseIn && m_isActive) {
        // Dessine le symbole du centre
        QPainter(this).drawPixmap(QRectF(static_cast<qreal>(m_currentMousePos.x() - w / 2),
                                         static_cast<qreal>(m_currentMousePos.y() - h / 2), w, h),
                                  m_centerPixmap, QRectF(0, 0, w, h));
    }
    delete painter;
}

void Overlay::abort() {
    m_isCenterSet = false;
    m_isActive = false;
    repaint();
}

void Overlay::activate() {
    m_isActive = true;
}

void Overlay::setCenterPosition(const QPoint& pos) {
    if (parent()) {
        if (parent()->isWidgetType()) {
            auto widget = static_cast<QWidget*>(parent());
            if (widget->width() > pos.x() && widget->height() > pos.y()) {
                m_centerPosition = pos;
                m_isCenterSet = true;
                return;
            }
        }
    }
    m_isCenterSet = false;
    repaint();
}

void Overlay::setCurrentMousePos(const QPoint& pos) {
    m_currentMousePos = pos;
    repaint();
}

void Overlay::setMouseIn(bool isIn) {
    m_isMouseIn = isIn;
    repaint();
}

qreal Overlay::angle() const {
    qreal angle = M_PI + qAtan2(static_cast<qreal>(m_centerPosition.y() - m_currentMousePos.y()),
                                static_cast<qreal>(m_centerPosition.x() - m_currentMousePos.x()));
    return angle;
}

const QPoint& Overlay::centerPos() const {
    return m_centerPosition;
}

const QPoint& Overlay::currentMousePos() const {
    return m_currentMousePos;
}

bool Overlay::isCenterSet() const {
    return m_isCenterSet;
}

OverlayFactoryFilter::OverlayFactoryFilter(QObject *parent) : QObject(parent) {}

OverlayFactoryFilter::~OverlayFactoryFilter() {}

bool OverlayFactoryFilter::eventFilter(QObject *obj, QEvent *event) {
    if (obj->isWidgetType()) {
        QWidget *widget = static_cast<QWidget*>(obj);
        if (event->type() == QEvent::MouseButtonRelease) {
            QMouseEvent *mouseEvent = static_cast<QMouseEvent*>(event);
            if (m_overlay->isCenterSet()) { // Deuxième clic
                QPoint center = m_overlay->centerPos();
                qreal angle = m_overlay->angle();
                m_overlay->abort();
                emit transformationReady(center, angle);
            } else { // Premier clic
                m_overlay->setCenterPosition(mouseEvent->pos());
            }
            m_overlay->setCurrentMousePos(mouseEvent->pos());
        } else if (event->type() == QEvent::MouseMove) {
            QMouseEvent *mouseEvent = static_cast<QMouseEvent*>(event);
            m_overlay->setCurrentMousePos(mouseEvent->pos());
        } else if (event->type() == QEvent::Leave) {
            m_overlay->setMouseIn(false);
        } else if (event->type() == QEvent::Enter) {
            m_overlay->setMouseIn(true);
        } else if (event->type() == QEvent::Resize) {
            if (m_overlay && m_overlay->parentWidget() == widget) {
                m_overlay->resize(widget->size());
            }
            m_overlay->repaint();
        }
    }
    return QObject::eventFilter(obj, event);
}

void OverlayFactoryFilter::handleAbortAnalysis() {
    m_overlay->abort();

}

void OverlayFactoryFilter::setParent(QObject *parent) {
    if (parent->isWidgetType()) {
        QWidget *p = static_cast<QWidget*>(parent);
        if (!m_overlay) {
            m_overlay = new Overlay;
        }
        m_overlay->activate();
        m_overlay->setParent(p);
        m_overlay->resize(p->size());
        m_overlay->show();
        QObject::setParent(parent);
    }
}
