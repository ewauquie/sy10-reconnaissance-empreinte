#ifndef INSPECT_ANALYSIS_OVERLAY_H
#define INSPECT_ANALYSIS_OVERLAY_H

// Code adapted from https://stackoverflow.com/a/19201908

#include <QMessageBox>
#include <QObject>
#include <QPainter>
#include <QPaintEvent>
#include <QPoint>
#include <QPointer>
#include <QRectF>
#include <QWidget>
#include <QtMath>

#include "Analyzer.hpp"
#include "Global.hpp"


// Classe capable de s'afficher par dessus d'autres Widgets

class InspectAnalysisOverlay : public QWidget {
public:
    explicit InspectAnalysisOverlay(QWidget *parent = nullptr);

    void activate(); // Active
    void setCurrentMousePos(const QPoint& pos); // Met à jour la position du curseur
    void setImageSize(const QSize& size);
    void setWorker(const Analyzer* worker);
    void setMouseIn(bool isIn);

    // Getters
    const QPoint& currentMousePos() const;
protected:
    void paintEvent(QPaintEvent *event) override;

    QPoint m_currentMousePos;
    QSize m_imageSize;
    const Analyzer *m_worker;
    bool m_isActive;
    bool m_isMouseIn;
};


// Classe servant d'interface pour les événements

class InspectAnalysisOverlayFactoryFilter : public QObject {
Q_OBJECT
public:
    explicit InspectAnalysisOverlayFactoryFilter(QObject *parent = nullptr);
    virtual ~InspectAnalysisOverlayFactoryFilter() override;
    void setParent(QObject *parent);
    void setImageSize(const QSize& size);
    void setWorker(const Analyzer* worker);
protected:
    bool eventFilter(QObject *obj, QEvent *event) override;

    QPointer<InspectAnalysisOverlay> m_overlay;
};

#endif // INSPECT_ANALYSIS_OVERLAY_H
