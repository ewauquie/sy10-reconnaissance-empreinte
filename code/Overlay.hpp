#ifndef OVERLAY_H
#define OVERLAY_H

// Code adapted from https://stackoverflow.com/a/19201908

#include <QMessageBox>
#include <QObject>
#include <QPainter>
#include <QPaintEvent>
#include <QPoint>
#include <QPointer>
#include <QRectF>
#include <QWidget>
#include <QtMath>


// Classe capable de s'afficher par dessus d'autres Widgets
//   Récolte la position du centre de l'image de l'empreinte
//   et l'angle de départ de l'analyse

class Overlay : public QWidget {
public:
    explicit Overlay(QWidget *parent = nullptr);

    void abort(); // Désactive et annule la collecte de positions
    void activate(); // Active
    void setCurrentMousePos(const QPoint& pos); // Met à jour la position du curseur
    void setCenterPosition(const QPoint& pos);
    void setMouseIn(bool isIn);

    // Getters
    qreal angle() const; // angle du segment entre le centre et la position du curseur
    const QPoint& currentMousePos() const;
    const QPoint& centerPos() const;
    bool isCenterSet() const;
protected:
    void paintEvent(QPaintEvent *event) override;

    QPoint m_currentMousePos;
    QPoint m_centerPosition;
    bool m_isActive;
    bool m_isCenterSet;
    bool m_isMouseIn;
    QPixmap m_centerPixmap; // Symbole du centre de l'empreinte
    QPixmap m_fingerprintSymbol; // Symbole d'empreinte digitale
};


// Classe servant d'interface pour les événements

class OverlayFactoryFilter : public QObject {
Q_OBJECT
public:
    explicit OverlayFactoryFilter(QObject *parent = nullptr);
    virtual ~OverlayFactoryFilter() override;
    void setParent(QObject *parent);
public slots:
    void handleAbortAnalysis(); // Relaie l'annulation à `m_overlay`
signals:
    void transformationReady(QPoint center, qreal angle);
protected:
    bool eventFilter(QObject *obj, QEvent *event) override;

    QPointer<Overlay> m_overlay;
};

#endif // OVERLAY_H
