#ifndef ANALYZER_H
#define ANALYZER_H

#include <QByteArray>
#include <QImage>
#include <QMap>
#include <QObject>
#include <QPainter>
#include <QPoint>
#include <QPointF>
#include <QVector2D>
#include <QtMath>

#include "fuzzylite/fl/Engine.h"

#include "EndingEngine.hpp"
#include "BifurcationEngine.hpp"
#include "Global.hpp"


// Classe chargée de l'analyse de l'empreinte digitale

class Analyzer : public QObject {
Q_OBJECT
public:
    Analyzer();

    QImage getMinutiaImage(MinutiaEngine::Engine engineType) const; // Génère une image représentant la position de la minutie reconnue par `engineType`

    QImage getSectionImage(int ring, int section) const; // Retourne la section de l'image aux coordonnées spécifiées
    void rasterize(const QImage& src, QImage *dest) const; // Réduit la résolution d'une image à la résolution de `dest`

    void getAnalysisCoordinates(qreal x, qreal y, int *ring, int* section) const;
    MinutiaEngine::Engine getEngine(const QString& engineName) const; // Retourne l'id du FIS `engineName`
    QString getResultsStr() const; // Retourne le résultat de l'analyse

public slots:
    void doLogPolarTransformation(const QImage& src, QPoint center, qreal angle); // Génère `m_transformedImage`
    void doRidgeDistanceCalculation(const QImage& src, QPoint center, qreal angle); // Calcule la taille d'un sillon en pixels (`m_ridgeDistance`)
    void doScanning(); // Réalise le scan de `m_transformedImage`, minutie par minutie

signals:
    void imageTransformed(const QImage* result);
    void scanningDone(const QImage* result);

protected:
    const QImage *m_sourceImage; // Copie de l'image source
    QImage *m_transformedImage; // Image transformée en coordonnées polaire logarithmiques
    QImage *m_rasterizedSection; // Section de `m_transformedImage` fournie à un FIS
    QPointF m_center; // Centre de `m_sourceImage` en pixels
    qreal m_angle; // Angle de départ ($\theta_0$)
    qreal m_radius; // Rayon $r$ utilisé pour la transformation polaire
    qreal m_ridgeDistance; // Distance entre deux sillons en pixels
    QVector<int> m_scanningDivision; // Tableau du nombre de scans pour chaque anneau

    QMap<MinutiaEngine::Engine, MinutiaEngine*> m_engines; // Conteneur des FIS pour chaque minutie
    QMap<MinutiaEngine::Engine, QColor*> m_minutiaColors; // Conteneur des couleurs associées à chaque minutie
    QMap<MinutiaEngine::Engine, QImage*> m_minutiaImages; // Conteneur des images overlay des minuties
    QVector<QVector<qreal>> m_results[static_cast<int>(MinutiaEngine::Engine::NUMBER_OF_ENGINE)]; // Tableau 2D (r, theta) par FIS

    QPointF logPolarToCartesian(const QPointF& src) const; // Converti des coordonnées polaires logarithmiques en coordonnées cartésiennes
    QPointF cartesianToLogPolar(const QPointF& src) const; // Converti des coordonnées cartésiennes en coordonnées polaires logarithmiques

    static uint bilinearInterpolation(const QPointF& px, const QImage& src); // Effectue une interpolation bilinéaire
    static void pasteOnImage(QImage& src1, const QImage& src2, QPoint pos); // Copie une image en nuances de gris sur une autre
};

#endif // ANALYZER_H
