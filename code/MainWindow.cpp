#include "MainWindow.hpp"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent),
      m_fileMenu(new QMenu(this)),
      m_mainLayout(new QGridLayout),
      m_minutiaButtonsLayout(new QGridLayout),
      m_scrollArea(new QScrollArea(this)),
      m_centralWidget(new QWidget(m_scrollArea->viewport())),
      m_candidateFingerprintHolder(new QLabel(this)),
      m_candidateFingerprintImage(new QImage),
      m_processedFingerprintHolder(new QLabel(this)),
      m_processedFingerprintImage(new QImage),
      m_loadCandidateFingerprintButton(new QPushButton),
      m_actionCandidateFingerprintButtons(new QStackedWidget),
      m_saveCandidateFingerprintButton(new QPushButton),
      m_analyzeCandidateFingerprintButton(new QPushButton),
      m_fingerprintTransformedHolder(new QLabel(this)),
      m_fingerprintTransformedImage(new QImage),
      m_fingerprintDB(new QStandardItemModel),
      m_fingerprintDBView(new QTableView(this)),
      m_worker(new Analyzer),
      m_factory(new OverlayFactoryFilter) {
    setWindowTitle(tr("Fingerprint scanner"));

    QObject::connect(m_loadCandidateFingerprintButton, &QPushButton::clicked, this, &MainWindow::promptCandidateFile);
    QObject::connect(this, &MainWindow::candidateImageLoaded, this, &MainWindow::showAnalyzeButton);
    QObject::connect(m_analyzeCandidateFingerprintButton, &QPushButton::clicked, this, &MainWindow::selectCenter);
    QObject::connect(m_scrollArea->horizontalScrollBar(), &QAbstractSlider::valueChanged, this, &MainWindow::moveView);
    QObject::connect(m_scrollArea->verticalScrollBar(), &QAbstractSlider::valueChanged, this, &MainWindow::moveView);
    QObject::connect(this, &MainWindow::abortAnalysis, m_factory, &OverlayFactoryFilter::handleAbortAnalysis);
    QObject::connect(m_factory, &OverlayFactoryFilter::transformationReady, this, &MainWindow::handleLaunchAnalysis);
    QObject::connect(this, &MainWindow::launchComparison, this, &MainWindow::compare);
    QObject::connect(m_saveCandidateFingerprintButton, &QPushButton::clicked, this, &MainWindow::saveAnalysis);

    // Créé un thread pour les opérations d'analyse
    m_worker->moveToThread(&m_workerThread);
    QObject::connect(&m_workerThread, &QThread::finished, m_worker, &QObject::deleteLater);
    QObject::connect(this, &MainWindow::launchRidgeDistanceCalculation, m_worker, &Analyzer::doRidgeDistanceCalculation);
    QObject::connect(this, &MainWindow::launchLogPolarTransformation, m_worker, &Analyzer::doLogPolarTransformation);
    QObject::connect(m_worker, &Analyzer::imageTransformed, this, &MainWindow::handleImageTransformed);
    QObject::connect(this, &MainWindow::launchScanning, m_worker, &Analyzer::doScanning);
    QObject::connect(m_worker, &Analyzer::scanningDone, this, &MainWindow::handleScanningDone);
    m_workerThread.start();

    // Construit la base de donnée
    QFile *dbFile = new QFile(DB_PATH);
    if (!dbFile->open(QIODevice::ReadWrite | QIODevice::Text)) {
        QMessageBox::critical(this, tr("Can't open file"), tr("Error : can't open \"") + DB_PATH + "\"\n" + tr("The database has been disabled"));
        m_dbStream = nullptr;
    } else {
        m_dbStream = new QTextStream(dbFile);
    }
    if (dbFile->exists() && dbFile->size() > 0) { // La base de donnée existe
        // Vérifie la version
        QString line;
        line = m_dbStream->readLine();
        if (line.split(":").size() == 2) {
            line = line.split(":")[1];
            if (line != DB_VERSION) {
                QMessageBox::critical(this, tr("Wrong database version"), tr("Error : the database has the wrong version") + "\n" + tr("The database has been disabled"));
                delete m_dbStream;
                m_dbStream = nullptr;
            }
        } else {
            QMessageBox::critical(this, tr("Wrong database version"), tr("Error : the database version has the wrong format") + "\n" + tr("The database has been disabled"));
            delete m_dbStream;
            m_dbStream = nullptr;
        }

        // Lis les données
        QStandardItem *item = nullptr;
        while (m_dbStream->readLineInto(&line)) {
            if (line != "") { // Ligne non vide
                if (line.at(0) == " " && item != nullptr) { // Morceau d'analyse de l'empreinte
                    item->appendRow(new QStandardItem(line.remove(0, 1)));
                } else { // Nom d'une empreinte
                    if (item != nullptr) {
                        m_fingerprintDB->appendRow(item);
                    }
                    line = line.split(":")[0];
                    item = new QStandardItem(line);
                }
            }
        }
        if (item != nullptr) {
            m_fingerprintDB->appendRow(item);
        }
    } else { // La base de donnée n'existe pas
        // Enregistre la version
        *m_dbStream << "version:" << DB_VERSION << endl;
    }

    // Dispose les éléments d'affichage
    createMenus();
    createWidgets();

  #ifdef INSPECT_ANALYSIS
    m_inspectAnalysis = false;
    m_inspectAnalysisFactory = new InspectAnalysisOverlayFactoryFilter;
  #endif
}

MainWindow::~MainWindow() {
    // Ferme le thread ouvert dans le constructeur
    m_workerThread.quit();
    m_workerThread.wait();
}

void MainWindow::createMenus() {
    m_fileMenu = menuBar()->addMenu(tr("&File"));
}

void MainWindow::createWidgets() {
    /* Déclare les widgets utilisés et leur disposition */


    // Mesures de la géométrie de l'écran
    QSize screenSize = QApplication::screens().at(0)->size();
    int width = 2 * qMax(screenSize.height(), screenSize.width()) / 3;
    int height = 2 * width / 3;
    this->resize(width, height);

    setCentralWidget(m_scrollArea); // Rend le contenu de la fenêtre déplaçable lorsqu'elle est trop petite
    m_scrollArea->setFrameStyle(QFrame::NoFrame);
    m_centralWidget->setLayout(m_mainLayout);
    m_centralWidget->resize(innerSize());
    m_centralWidget->setMaximumWidth(7 * innerSize().width() / 6);
    m_centralWidget->setMaximumHeight(m_centralWidget->height());
    m_scrollArea->viewport()->resize(m_centralWidget->size());

    m_mainLayout->setColumnMinimumWidth(0, m_centralWidget->width() / 3);
    m_mainLayout->setColumnMinimumWidth(1, m_centralWidget->width() / 3);
    m_mainLayout->setColumnMinimumWidth(2, m_centralWidget->width() / 3);
    m_mainLayout->setColumnStretch(2, 1); // Troisième colonne étirable
    m_mainLayout->setRowMinimumHeight(0, 3 * m_centralWidget->height() / 8);
    m_mainLayout->setRowMinimumHeight(1, 4 * m_centralWidget->height() / 8);
    m_mainLayout->addLayout(m_minutiaButtonsLayout, 2, 2);

    m_mainLayout->addWidget(m_candidateFingerprintHolder, 0, 0, 2, 1);
    m_candidateFingerprintHolder->resize(m_centralWidget->width() / 3, 7 * m_centralWidget->height() / 8);
    m_candidateFingerprintHolder->setFrameShape(QFrame::StyledPanel);

    m_mainLayout->addWidget(m_processedFingerprintHolder, 0, 1, 2, 1);
    m_processedFingerprintHolder->resize(m_centralWidget->width() / 3, 7 * m_centralWidget->height() / 8);
    m_processedFingerprintHolder->setFrameShape(QFrame::StyledPanel);

    m_mainLayout->addWidget(m_fingerprintTransformedHolder, 0, 2);
    m_fingerprintTransformedHolder->resize(m_centralWidget->width() / 3, 3 * m_centralWidget->height() / 8);
    m_fingerprintTransformedHolder->setScaledContents(true);
    m_fingerprintTransformedHolder->setFrameShape(QFrame::StyledPanel);
    m_fingerprintTransformedHolder->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Preferred);

    m_mainLayout->addWidget(m_fingerprintDBView, 1, 2);
    m_fingerprintDBView->resize(m_centralWidget->width() / 3, 4 * m_centralWidget->height() / 8);
    m_fingerprintDBView->setFrameShape(QFrame::StyledPanel);
    m_fingerprintDBView->setModel(m_fingerprintDB);
    m_fingerprintDBView->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Preferred);


    m_mainLayout->addWidget(m_loadCandidateFingerprintButton, 2, 0, Qt::AlignTop);
    m_loadCandidateFingerprintButton->setMinimumHeight(2 * m_loadCandidateFingerprintButton->sizeHint().height());
    m_loadCandidateFingerprintButton->setText(tr("Load candidate image"));

    m_mainLayout->addWidget(m_actionCandidateFingerprintButtons, 2, 2, Qt::AlignTop);

    m_actionCandidateFingerprintButtons->addWidget(m_saveCandidateFingerprintButton);
    m_saveCandidateFingerprintButton->setText(tr("Save"));
    m_saveCandidateFingerprintButton->setMinimumHeight(2 * m_saveCandidateFingerprintButton->sizeHint().height()); // 2 fois plus haut que la normale
    m_saveCandidateFingerprintButton->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Preferred);

    m_actionCandidateFingerprintButtons->addWidget(m_analyzeCandidateFingerprintButton);
    m_analyzeCandidateFingerprintButton->setText(tr("Start analysis"));
    m_analyzeCandidateFingerprintButton->setMinimumHeight(2 * m_analyzeCandidateFingerprintButton->sizeHint().height());
    m_analyzeCandidateFingerprintButton->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Preferred);
    m_actionCandidateFingerprintButtons->hide();

    m_mainLayout->setSizeConstraint(QLayout::SetMinimumSize);
}

void MainWindow::compare(const Analyzer *worker) {
    std::set<MinutiaEngine::Minutia, MinutiaEngine::minutiaComp> minutiaList;

    if (!m_fingerprintDB) {
        QMessageBox::critical(this, tr("Database disabled"), tr("Error: the database is disabled because it is unusable"));
    } else {
        QString resultsString = worker->getResultsStr();
        QTextStream resultsStream(&resultsString);
        QString resultLine;
        // Liste les minuties
        while (resultsStream.readLineInto(&resultLine)) {
            resultLine.remove(0, 1);
            // Enregistre la minutie
            MinutiaEngine::Minutia m;
            m.type = static_cast<MinutiaEngine::Engine>(resultLine.split("#")[0].toInt());
            resultLine = resultLine.split("#")[1];
            m.ring = resultLine.split("_")[0].toInt();
            resultLine = resultLine.split("_")[1];
            m.section = resultLine.split(":")[0].toInt();
            resultLine = resultLine.split(":")[1];
            m.membership = resultLine.toDouble();
            minutiaList.insert(m);
        }
        // Compare les `COMPARE_COUNT` minuties les plus présentes
        CompareEngine compareEngine;
        std::set<MinutiaEngine::Minutia, MinutiaEngine::minutiaComp>::iterator it(minutiaList.begin());
        for (int i(0); i < COMPARE_COUNT; ++i) {
            compareEngine.setMinutia(i, *it);
            ++it;
        }
        // Compare à chaque minutie connue
        QList<QStandardItem*> newColumn;
        for (int i(0); i < m_fingerprintDB->rowCount(); ++i) { // Chaque empreinte
            QStandardItem *fingerprint = m_fingerprintDB->item(i);
            QList<qreal> closeness;

            for (int j(0); j < fingerprint->rowCount(); ++j) { // Chaque minutie
                QStandardItem *minutia = fingerprint->child(j);
                QString text = minutia->text();
                MinutiaEngine::Minutia m;
                m.type = static_cast<MinutiaEngine::Engine>((text.split("#")[0].toInt()));
                text = text.split("#")[1];
                m.ring = text.split("_")[0].toInt();
                text = text.split("_")[1];
                m.section = text.split(":")[0].toInt();
                text = text.split(":")[1];
                m.membership = text.toDouble();
                compareEngine.setDBMinutia(m);
                compareEngine.process();
                QString proximity = QString::fromStdString(compareEngine.getOutputVariable("<proximity>")->fuzzyOutputValue());
                closeness.append(proximity.split("/")[0].toDouble());
            }
            std::sort(closeness.begin(), closeness.end());
            closeness.erase(closeness.begin(), closeness.end() - COMPARE_COUNT);
            qreal comparedProximity(0);
            for (QList<qreal>::iterator it(closeness.begin()); it != closeness.end(); ++it) {
                comparedProximity += *it;
            }
            comparedProximity /= COMPARE_COUNT;

            newColumn.append(new QStandardItem(QString::number(comparedProximity)));
        }
        m_fingerprintDB->appendColumn(newColumn);
    }
}

void MainWindow::promptCandidateFile() {
    QString filename = QFileDialog::getOpenFileName(this, tr("Open Candidate Image"), QDir::currentPath());
    loadCandidateFile(filename);
}

void MainWindow::showAnalyzeButton() {
    m_actionCandidateFingerprintButtons->setCurrentIndex(1);
    m_actionCandidateFingerprintButtons->show();
}

void MainWindow::saveAnalysis() {
    if (!m_fingerprintDB) {
        QMessageBox::critical(this, tr("Database disabled"), tr("Error: the database is disabled because it is unusable"));
    } else {
        QString name = QInputDialog::getText(this, tr("Fingerprint's name"), tr("Fingerprint's name"));
        *m_dbStream << name << ":" << endl;
        *m_dbStream << m_worker->getResultsStr() << endl;
    }
}

void MainWindow::selectCenter() {
    /* Lance la sélection du centre de l'empreinte
     *
     * Installe un overlay sur `m_candidateFingerprintHolder`
     */
    m_analyzeCandidateFingerprintButton->setDisabled(true);
    m_candidateFingerprintHolder->installEventFilter(m_factory);
    m_candidateFingerprintHolder->setMouseTracking(true);
    m_factory->setParent(m_candidateFingerprintHolder);
}

void MainWindow::handleLaunchAnalysis(QPoint center, qreal angle) {
    /* Prépare et déclenche l'analyse de l'empreinte dans l'`Analyzer`
     *
     * Ajuste la position de `center` en fonction de l'affichage de l'image de l'empreinte
     * dans `m_candidateFingerprintHolder`.
     */
    m_candidateFingerprintHolder->removeEventFilter(m_factory);
    m_candidateFingerprintHolder->setMouseTracking(false);
    if (m_candidateFingerprintImage->width() * m_candidateFingerprintHolder->height() < m_candidateFingerprintImage->height() * m_candidateFingerprintHolder->width()) {
        // L'image `m_candidateFingerprintImage` est plus _étroite_ que son support `m_candidateFingerprintHolder`
        qreal scale = static_cast<qreal>(m_candidateFingerprintImage->height()) / static_cast<qreal>(m_candidateFingerprintHolder->height());
        center.setX(static_cast<int>(scale * (center.x() - static_cast<qreal>(m_candidateFingerprintHolder->width()) / 2) +
                                     static_cast<qreal>(m_candidateFingerprintImage->width()) / 2));
        center.setY(static_cast<int>(scale * center.y()));
    } else {
        // L'image `m_candidateFingerprintImage` est plus _large_ que son support `m_candidateFingerprintHolder`
        qreal scale = static_cast<qreal>(m_candidateFingerprintImage->width()) / static_cast<qreal>(m_candidateFingerprintHolder->width());
        center.setX(static_cast<int>(scale * center.x()));
        center.setY(static_cast<int>(scale * (center.y() - static_cast<qreal>(m_candidateFingerprintHolder->height()) / 2) +
                                     static_cast<qreal>(m_candidateFingerprintImage->height()) / 2));
    }


  #ifdef INSPECT_ANALYSIS
    m_inspectAnalysis = false;
  #endif

    emit launchRidgeDistanceCalculation(*m_candidateFingerprintImage, center, angle);
    emit launchLogPolarTransformation(*m_candidateFingerprintImage, center, angle);
}

void MainWindow::handleImageTransformed(const QImage *out) {
    delete m_fingerprintTransformedImage;
    m_fingerprintTransformedImage = new QImage(*out);
    loadImageToLabel(m_fingerprintTransformedImage, m_fingerprintTransformedHolder);
    emit launchScanning();
}

void MainWindow::handleScanningDone(const QImage* out) {
    // Réactive le bouton d'analyse
    m_analyzeCandidateFingerprintButton->setDisabled(false);
    // Affiche l'empreinte avec les minuties
    if (m_processedFingerprintImage) { delete m_processedFingerprintImage; }
    m_processedFingerprintImage = new QImage(*out);
    loadImageToLabel(m_processedFingerprintImage, m_processedFingerprintHolder);

  #ifdef INSPECT_ANALYSIS
    m_inspectAnalysis = true;

    m_processedFingerprintHolder->installEventFilter(m_inspectAnalysisFactory);
    m_processedFingerprintHolder->setMouseTracking(true);
    m_inspectAnalysisFactory->setParent(m_processedFingerprintHolder);
    m_inspectAnalysisFactory->setImageSize(m_processedFingerprintImage->size());
    m_inspectAnalysisFactory->setWorker(m_worker);
  #endif

    m_actionCandidateFingerprintButtons->setCurrentIndex(0);
    emit launchComparison(m_worker);
}

void MainWindow::moveView(int) {
    m_centralWidget->move(- m_scrollArea->horizontalScrollBar()->value(),
                          - m_scrollArea->verticalScrollBar()->value());
}

void MainWindow::updatePixmap(QLabel *label) {
    // Code from https://stackoverflow.com/a/8212120
    QPixmap p = *(label->pixmap());
    int w = label->width();
    int h = label->height();

    label->setPixmap(p.scaled(w, h, Qt::KeepAspectRatio));
}

void MainWindow::keyPressEvent(QKeyEvent *event) {
    if (event->key() == Qt::Key_Escape) {
        m_analyzeCandidateFingerprintButton->setDisabled(false);
        m_candidateFingerprintHolder->removeEventFilter(m_factory);
        m_candidateFingerprintHolder->setMouseTracking(false);
        emit abortAnalysis();
    }
}

void MainWindow::resizeEvent(QResizeEvent*) {
    m_centralWidget->resize(innerSize());
    m_centralWidget->move(- m_scrollArea->horizontalScrollBar()->value(),
                          - m_scrollArea->verticalScrollBar()->value());
    int scrollBarHeight = m_scrollArea->horizontalScrollBar()->isVisible() ? m_scrollArea->horizontalScrollBar()->height() : 0;
    int scrollBarWidth = m_scrollArea->verticalScrollBar()->isVisible() ? m_scrollArea->verticalScrollBar()->width() : 0;
    m_scrollArea->horizontalScrollBar()->setPageStep(m_scrollArea->viewport()->width() + scrollBarWidth);
    m_scrollArea->verticalScrollBar()->setPageStep(m_scrollArea->viewport()->height() + scrollBarHeight);
    m_scrollArea->horizontalScrollBar()->setRange(0, m_centralWidget->width() - m_scrollArea->horizontalScrollBar()->pageStep());
    m_scrollArea->verticalScrollBar()->setRange(0, m_centralWidget->height() - m_scrollArea->verticalScrollBar()->pageStep());
    m_scrollArea->horizontalScrollBar()->setValue(- m_centralWidget->x());
    m_scrollArea->verticalScrollBar()->setValue(- m_centralWidget->y());
}

void MainWindow::loadCandidateFile(const QString& filename) {
    if (!filename.isEmpty()) {
        if (!m_candidateFingerprintImage->load(filename)) {
            QMessageBox::critical(this, tr("Error opening ") + filename, tr("Error: ") + filename + tr(" is not a file, or not a valid image"));
        } else {
            loadImageToLabel(m_candidateFingerprintImage, m_candidateFingerprintHolder);
            emit candidateImageLoaded();
        }
    }
}

void MainWindow::loadImageToLabel(const QImage* src, QLabel* dest) {
    QPixmap p = QPixmap::fromImage(*src);
    p.scaled(dest->width(), dest->height(), Qt::KeepAspectRatio);
    dest->setPixmap(p);
    updatePixmap(dest);
}

QSize MainWindow::innerSize() const {
    QSize size = this->size();
    if (menuWidget()) {
        QMenuBar *menuBar = static_cast<QMenuBar*>(menuWidget());
        if (!menuBar->isNativeMenuBar()) {
            size.setHeight(size.height() - menuBar->height());
        }
    }
    return size;
}
