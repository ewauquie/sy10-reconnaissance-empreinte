#include <cstdlib>      // EXIT_SUCCESS, EXIT_FAILURE
#include <QTextStream>
#include <QApplication>
#include <QImage>

#include "MainWindow.hpp"


int main(int argc, char *argv[]) {
    // Vérifie les arguments fournis en ligne de commande
    if (argc > 3) {
        QTextStream(stdout) << QObject::tr("Too many arguments") << endl;
        QTextStream(stdout) << QObject::tr("Usage: ") << argv[0] << QObject::tr(" [candidate [reference]]") << endl;
        return EXIT_FAILURE;
    }

    // Charge les fichiers
    QImage candidate;
    QImage reference;
    if (argc >= 2) {
        if (!candidate.load(argv[1])) {
            QTextStream(stdout) << QObject::tr("Error: ") << argv[1] << QObject::tr(" is not a file, or not a valid image") << endl;
            return EXIT_FAILURE;
        }

        if (argc == 3) {
            if (!reference.load(argv[2])) {
                QTextStream(stdout) << QObject::tr("Error: ") << argv[2] << QObject::tr(" is not a file, or not a valid image") << endl;
                return EXIT_FAILURE;
            }
        }
    }

    // Charge l'application
    QApplication app(argc, argv);
    MainWindow w;
    w.show();

    // Qt event loop
    int returnValue = app.exec();

    return returnValue;
}
