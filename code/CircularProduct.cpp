#include "CircularProduct.hpp"

namespace fl {

    std::string CircularProduct::className() const {
        return "CircularProduct";
    }

    Complexity CircularProduct::complexity() const {
        return Complexity().arithmetic(1);
    }

    scalar CircularProduct::compute(scalar a, scalar b) const {
        a = 1 - a;
        b = 1 - b;
        return std::max(scalar(0.0), 1 - sqrt(a * a + b * b));
    }

    CircularProduct* CircularProduct::clone() const {
        return new CircularProduct(*this);
    }

    TNorm* CircularProduct::constructor() {
        return new CircularProduct;
    }

}
