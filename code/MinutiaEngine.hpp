#ifndef MINUTIA_ENGINE_HPP
#define MINUTIA_ENGINE_HPP

#include <QImage>

#include <QtMath>

#include "fuzzylite/fl/Engine.h"
#include "fuzzylite/fl/defuzzifier/WeightedAverage.h"
#include "fuzzylite/fl/rule/Rule.h"
#include "fuzzylite/fl/rule/RuleBlock.h"
#include "fuzzylite/fl/term/Ramp.h"
#include "fuzzylite/fl/term/Trapezoid.h"
#include "fuzzylite/fl/norm/s/Maximum.h"
#include "fuzzylite/fl/variable/InputVariable.h"
#include "fuzzylite/fl/variable/OutputVariable.h"


// Classe abstraite des FIS

class MinutiaEngine : public fl::Engine {
public:
    enum class Engine {Ending, Bifurcation, NUMBER_OF_ENGINE}; // Liste des FIS

    struct Minutia {
        MinutiaEngine::Engine type;
        int ring;
        int section;
        qreal membership;
    };
    struct minutiaComp {
        bool operator() (const Minutia& lhs, const Minutia& rhs) const {
            return lhs.membership < rhs.membership;
        }
    };


    MinutiaEngine();
    MinutiaEngine(int dataSize);

    virtual int dataSize() const = 0; // Résolution de l'image analysée
    virtual MinutiaEngine::Engine engineType() const = 0; // Retourne le type de FIS
    virtual void processImage(const QImage *src); // Lance le calcul des règles floues
    virtual qreal value() const; // Retiourne le degré de présence de la minutie reconnue par le FIS

protected:
    void extendRule(std::string rule); // Reproduit une règle par symmétries
    std::string rotRule(std::string rule) const; // Applique une rotation de 90 deg à `rule`
    std::string mirrorRule(std::string rule) const; // Applique une symétrie axiale à `rule`
};

#endif // MINUTIA_ENGINE_HPP
