#include "InspectAnalysisOverlay.hpp"

InspectAnalysisOverlay::InspectAnalysisOverlay(QWidget *parent)
    : QWidget(parent),
      m_worker(nullptr),
      m_isActive(false),
      m_isMouseIn(false) {
    setAttribute(Qt::WA_NoSystemBackground);
    setAttribute(Qt::WA_TransparentForMouseEvents);
}

void InspectAnalysisOverlay::paintEvent(QPaintEvent*) {
    if (parent()->isWidgetType()) {
        QWidget *p = static_cast<QWidget*>(parent());

        if (m_isMouseIn && m_isActive) {
            QPainter *painter = new QPainter(this);

            // Converti les coordonnées de la souris
            int cartX, cartY; // Coordonnées cartésiennes de l'empreinte
            if (m_imageSize.width() * p->height() < m_imageSize.height() * p->width()) {
                // L'image est plus _étroite_ que son support `p`
                qreal scale = static_cast<qreal>(m_imageSize.height()) / static_cast<qreal>(p->height());
                cartX = static_cast<int>(scale * (m_currentMousePos.x() - static_cast<qreal>(p->width()) / 2) +
                                         static_cast<qreal>(m_imageSize.width()) / 2);
                cartY = static_cast<int>(scale * m_currentMousePos.y());
            } else {
                // L'image est plus _large_ que son support `p`
                qreal scale = static_cast<qreal>(m_imageSize.width()) / static_cast<qreal>(p->width());
                cartX = static_cast<int>(scale * m_currentMousePos.x());
                cartY = static_cast<int>(scale * (m_currentMousePos.y() - static_cast<qreal>(p->height()) / 2) +
                                         static_cast<qreal>(m_imageSize.height()) / 2);
            }

            int ring, section; // Coordonnées d'analyse de l'empreinte
            m_worker->getAnalysisCoordinates(cartX, cartY, &ring, &section);

            if (ring != -1) { // Si le curseur est à un emplacement valide
                // Récupère l'image
                QImage sectionImage(m_worker->getSectionImage(ring, section));
                QImage *analyzedImage = new QImage(ANALYZED_IMAGE_DEFAULT_RES, ANALYZED_IMAGE_DEFAULT_RES, QImage::Format_Indexed8);
                for (int i(0); i < 256; ++i) {
                    analyzedImage->setColor(i, qRgb(i, i, i));
                }
                m_worker->rasterize(sectionImage, analyzedImage);

                // Dessine l'image
                QRectF targetRect;
                targetRect.setX(m_currentMousePos.x() - PREVIEW_SIZE - 1);
                targetRect.setY(m_currentMousePos.y() - 2 * PREVIEW_SIZE - 10);
                targetRect.setWidth(PREVIEW_SIZE * 2);
                targetRect.setHeight(PREVIEW_SIZE * 2);

                painter->drawPixmap(targetRect, QPixmap::fromImage(*analyzedImage), analyzedImage->rect());
            }
            delete painter;
        }
    }
}

void InspectAnalysisOverlay::activate() {
    m_isActive = true;
}

void InspectAnalysisOverlay::setCurrentMousePos(const QPoint& pos) {
    m_currentMousePos = pos;
    repaint();
}

void InspectAnalysisOverlay::setImageSize(const QSize& size) {
    m_imageSize = size;
}

void InspectAnalysisOverlay::setWorker(const Analyzer *worker) {
    m_worker = worker;
}

void InspectAnalysisOverlay::setMouseIn(bool isIn) {
    m_isMouseIn = isIn;
    repaint();
}

const QPoint& InspectAnalysisOverlay::currentMousePos() const {
    return m_currentMousePos;
}

InspectAnalysisOverlayFactoryFilter::InspectAnalysisOverlayFactoryFilter(QObject *parent) : QObject(parent) {}

InspectAnalysisOverlayFactoryFilter::~InspectAnalysisOverlayFactoryFilter() {}

bool InspectAnalysisOverlayFactoryFilter::eventFilter(QObject *obj, QEvent *event) {
    if (obj->isWidgetType()) {
        QWidget *widget = static_cast<QWidget*>(obj);
        if (event->type() == QEvent::MouseMove) {
            QMouseEvent *mouseEvent = static_cast<QMouseEvent*>(event);
            m_overlay->setCurrentMousePos(mouseEvent->pos());
        } else if (event->type() == QEvent::Leave) {
            m_overlay->setMouseIn(false);
        } else if (event->type() == QEvent::Enter) {
            m_overlay->setMouseIn(true);
        } else if (event->type() == QEvent::Resize) {
            if (m_overlay && m_overlay->parentWidget() == widget) {
                m_overlay->resize(widget->size());
            }
            m_overlay->repaint();
        }
    }
    return QObject::eventFilter(obj, event);
}

void InspectAnalysisOverlayFactoryFilter::setParent(QObject *parent) {
    if (parent->isWidgetType()) {
        QWidget *p = static_cast<QWidget*>(parent);
        if (!m_overlay) {
            m_overlay = new InspectAnalysisOverlay;
        }
        m_overlay->activate();
        m_overlay->setParent(p);
        m_overlay->resize(p->size());
        m_overlay->show();
        QObject::setParent(parent);
    }
}

void InspectAnalysisOverlayFactoryFilter::setImageSize(const QSize &size) {
    if (m_overlay) {
        m_overlay->setImageSize(size);
    }
}

void InspectAnalysisOverlayFactoryFilter::setWorker(const Analyzer *worker) {
    if (m_overlay) {
        m_overlay->setWorker(worker);
    }
}
