#include"BifurcationEngine.hpp"

BifurcationEngine::BifurcationEngine() : MinutiaEngine(dataSize()) {
    setName("BifurcationMinutia");

    fl::OutputVariable *angle = new fl::OutputVariable;
    angle->setName("<angle>");
    angle->setEnabled(true);
    angle->setRange(0, 360);
    angle->setLockValueInRange(true);
    angle->setAggregation(new fl::Maximum);
    angle->setDefuzzifier(new fl::WeightedAverage); // Non utilisé
    angle->setDefaultValue(fl::nan);
    angle->setLockPreviousValue(false);
    angle->addTerm(new fl::ReverseTrapezoid("'0'", 0, 15, 345, 360));
    angle->addTerm(new fl::Triangle("'1'", 12, 27, 45));
    angle->addTerm(new fl::Triangle("'2'", 48, 63, 78));
    angle->addTerm(new fl::Triangle("'3'", 75, 90, 105));
    angle->addTerm(new fl::Triangle("'4'", 102, 117, 132));
    angle->addTerm(new fl::Triangle("'5'", 138, 153, 168));
    angle->addTerm(new fl::Triangle("'6'", 165, 180, 195));
    angle->addTerm(new fl::Triangle("'7'", 192, 207, 222));
    angle->addTerm(new fl::Triangle("'8'", 228, 243, 258));
    angle->addTerm(new fl::Triangle("'9'", 255, 270, 285));
    angle->addTerm(new fl::Triangle("'10'", 282, 297, 312));
    angle->addTerm(new fl::Triangle("'11'", 318, 333, 358));
    addOutputVariable(angle);

    fl::RuleBlock *ruleBlock = new fl::RuleBlock;
    ruleBlock->setName("BifurcationRule");
    ruleBlock->setConjunction(new fl::Minimum);
    ruleBlock->setDisjunction(new fl::DrasticSum);
    ruleBlock->setImplication(new fl::Minimum);
    ruleBlock->setActivation(new fl::General);

    std::string antecedent1 = "if (<pixel_2x2> is 'white' or <pixel_2x2> is 'gray') "
                              "and <pixel_0x1> is 'black' "
                              "and <pixel_0x2> is 'black' "
                              "and <pixel_0x3> is 'black' "
                              "and <pixel_0x4> is 'black' "
                              "and <pixel_1x0> is 'black' "
                              "and (<pixel_1x1> is 'black' or <pixel_1x1> is 'gray') "
                              "and <pixel_1x4> is 'black' "
                              "and <pixel_2x4> is 'black' "
                              "and (<pixel_3x0> is 'white' or <pixel_3x0> is 'gray') "
                              "and (<pixel_3x2> is 'black' or <pixel_3x2> is 'gray' "
                              "or <pixel_3x3> is 'black' or <pixel_3x3> is 'gray') "
                              "and (<pixel_4x1> is 'black' or <pixel_4x2> is 'black' or <pixel_4x3> is 'black') ";
    std::string antecedent2 = "if (<pixel_0x2> is 'black' or <pixel_0x2> is 'gray') "
                              "and <pixel_0x3> is 'black' "
                              "and <pixel_0x4> is 'black' "
                              "and (<pixel_1x0> is 'black' or <pixel_1x0> is 'gray') "
                              "and <pixel_1x1> is 'black' "
                              "and <pixel_1x2> is 'black' "
                              "and (<pixel_1x3> is 'black' or <pixel_1x3> is 'gray') "
                              "and <pixel_2x0> is 'black' "
                              "and (<pixel_2x1> is 'black' or <pixel_2x1> is 'gray') "
                              "and <pixel_2x3> is 'white' "
                              "and <pixel_2x4> is 'white' "
                              "and (<pixel_3x0> is 'black' or <pixel_3x0> is 'gray') "
                              "and <pixel_3x1> is 'black' "
                              "and <pixel_3x2> is 'black' "
                              "and (<pixel_3x3> is 'black' or <pixel_3x3> is 'gray') "
                              "and (<pixel_4x2> is 'black' or <pixel_4x2> is 'gray') "
                              "and <pixel_4x3> is 'black' "
                              "and <pixel_4x4> is 'black' ";
    std::string valueRule1 = antecedent1 + "then <value> is 'value'";
    std::string valueRule2 = antecedent1 + "then <value> is 'value'";

    ruleBlock->addRule(fl::Rule::parse(valueRule1, this));
    ruleBlock->addRule(fl::Rule::parse(valueRule2, this));
    addRuleBlock(ruleBlock);

    extendRule(valueRule1);
    extendRule(valueRule2);
}

int BifurcationEngine::dataSize() const {
    return 5;
}

MinutiaEngine::Engine BifurcationEngine::engineType() const {
    return MinutiaEngine::Engine::Bifurcation;
}

void BifurcationEngine::processImage(const QImage *src) {
    MinutiaEngine::processImage(src);
    if (src->width() == dataSize() && src->height() == dataSize()) {

        // Lance le calcul des règles floues
        std::string status;
        if (not isReady(&status)) {
            throw fl::Exception("Error: BifurcationEngine is not ready:\n" + status, FL_AT);
        }
        this->process();
    }
}
