#ifndef COMPARE_ENGINE_HPP
#define COMPARE_ENGINE_HPP

#include "fuzzylite/fl/Engine.h"
#include "fuzzylite/fl/norm/s/Maximum.h"
#include "fuzzylite/fl/norm/t/Minimum.h"
#include "fuzzylite/fl/rule/Rule.h"
#include "fuzzylite/fl/rule/RuleBlock.h"
#include "fuzzylite/fl/term/Discrete.h"
#include "fuzzylite/fl/term/Ramp.h"
#include "fuzzylite/fl/term/Trapezoid.h"
#include "fuzzylite/fl/term/Triangle.h"
#include "fuzzylite/fl/variable/InputVariable.h"
#include "fuzzylite/fl/variable/OutputVariable.h"
#include "CircularProduct.hpp"

#include <QString>

#include <QtMath>

#include "MinutiaEngine.hpp"
#include "Global.hpp"


// FIS comparant les résultats d'empreinte

class CompareEngine : public fl::Engine {
public:
    CompareEngine();
    void setMinutia(int index, const MinutiaEngine::Minutia& minutia);
    void setDBMinutia(const MinutiaEngine::Minutia& minutia);
};

#endif // COMPARE_ENGINE_HPP
