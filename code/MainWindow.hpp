#ifndef MAIN_WINDOW_H
#define MAIN_WINDOW_H

#include <set>

#include <QMainWindow>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QAction>
#include <QApplication>
#include <QDir>
#include <QFileDialog>
#include <QFileInfo>
#include <QFrame>
#include <QInputDialog>
#include <QImage>
#include <QKeyEvent>
#include <QLabel>
#include <QMenuBar>
#include <QMessageBox>
#include <QPixmap>
#include <QPushButton>
#include <QResizeEvent>
#include <QScreen>
#include <QScrollArea>
#include <QScrollBar>
#include <QSizePolicy>
#include <QStackedWidget>
#include <QString>
#include <QStandardItemModel>
#include <QTableView>
#include <QTextStream>
#include <QThread>
#include <QtMath>

#include "Analyzer.hpp"
#include "CompareEngine.hpp"
#include "Overlay.hpp"
#include "InspectAnalysisOverlay.hpp"
#include "Global.hpp"

// Fenêtre principale, classe chargée de l'affichage graphique et de l'interation utilisateur

class MainWindow : public QMainWindow {
Q_OBJECT
public:
    MainWindow(QWidget *parent = nullptr);
    virtual ~MainWindow() override;

public slots:
    void compare(const Analyzer *worker); // Compare l'empreinte analysée à la base de donnée
    void promptCandidateFile(); // Demande l'image d'empreinte digitale à analyser
    void showAnalyzeButton(); // Affiche le bouton d'analyse
    void handleImageTransformed(const QImage* out); // Affiche l'image transformée
    void handleLaunchAnalysis(QPoint center, qreal angle); // Déclenche l'analyse de l'empreinte
    void handleScanningDone(const QImage* out); // Redonne la main à l'utilisateur
    void saveAnalysis();
    void selectCenter(); // Active la sélection du centre de l'empreinte
    void moveView(int); // Déplace le contenu de la fenêtre

signals:
    void candidateImageLoaded(); // L'empreinte à été chargée
    void launchLogPolarTransformation(const QImage& src, QPoint center, qreal angle); // Lance la transformation polaire logarithmique
    void launchRidgeDistanceCalculation(const QImage& src, QPoint center, qreal angle); // Lance le calcul de la distance entre deux sillons
    void launchScanning(); // Lance l'analyse de l'empreinte
    void launchComparison(const Analyzer* worker);
    void abortAnalysis(); // Annule la sélection du centre et de l'angle par l'utilisateur

protected:
    void createMenus(); // Déclare les éléments du menu
    void createWidgets(); // Déclare les widgets de la fenêtre
    void loadCandidateFile(const QString& filename); // Charge l'image d'empreinte digitale à analyser

    static void updatePixmap(QLabel *label); // Ajuste l'affichage de l'image contenue dans `label`
    static void loadImageToLabel(const QImage* src, QLabel* dest); // Charge `src` dans `dest`

    QSize innerSize() const; // Renvoie la taille de la fenêtre sans la barre de menu et la barre de status

    void keyPressEvent(QKeyEvent *event) override;
    void resizeEvent(QResizeEvent*) override;


    QMenu *m_fileMenu;

    QGridLayout *m_mainLayout; // Disposition principale (grille 3x3)
    QGridLayout *m_minutiaButtonsLayout; // Disposition des boutons des minuties (grille 2x5)

    QScrollArea *m_scrollArea;
    QWidget *m_centralWidget;
    QLabel *m_candidateFingerprintHolder; // Support de l'image de l'empreinte à analyser
    QImage *m_candidateFingerprintImage; // Image de l'empreinte à analyser
    QLabel *m_processedFingerprintHolder; // Support de l'image de l'empreinte analysée
    QImage *m_processedFingerprintImage; // Image de l'empreinte analysée
    QPushButton *m_loadCandidateFingerprintButton; // Bouton de chargement de l'image de l'empreinte à analyser
    QStackedWidget *m_actionCandidateFingerprintButtons;
    QPushButton *m_saveCandidateFingerprintButton; // Bouton de sauvegarde de l'empreinte analysée dans la base de donnée
    QPushButton *m_analyzeCandidateFingerprintButton; // Bouton d'analyse de l'empreinte
    QLabel *m_fingerprintTransformedHolder; // Support de l'image transformée en coordonnées polaires logarithmiques
    QImage *m_fingerprintTransformedImage; // Image transformée en coordonnées polaires logarithmiques
    QPushButton (*m_minutiaButtons)[10]; // Tableau des boutons des minuties

    QStandardItemModel *m_fingerprintDB; // Base de donnée des empreintes analysées
    QTextStream *m_dbStream; // Flux du fichier de base de donnée. Vaut `nullptr` si la base de donnée est inutilisable

    QTableView *m_fingerprintDBView; // Vue de la base de donnée

    Analyzer *m_worker;
    QThread m_workerThread; // Thread contenant un objet `Analyzer`
    OverlayFactoryFilter *m_factory;

  #ifdef INSPECT_ANALYSIS
    InspectAnalysisOverlayFactoryFilter *m_inspectAnalysisFactory;
    bool m_inspectAnalysis; // `true` si l'inspection de l'analyse est active
  #endif
};

#endif // MAIN_WINDOW_H
