#ifndef REVERSETRAPEZOID_HPP
#define REVERSETRAPEZOID_HPP

#include "fuzzylite/fl/term/Term.h"
#include "fuzzylite/fl/term/Trapezoid.h"

namespace fl {
    class FL_API ReverseTrapezoid : public Term {
    public:
        explicit ReverseTrapezoid(const std::string& name = "",
                                  scalar vertexA = fl::nan,
                                  scalar vertexB = fl::nan,
                                  scalar vertexC = fl::nan,
                                  scalar vertexD = fl::nan,
                                  scalar height = 1.0);
        virtual ~ReverseTrapezoid() FL_IOVERRIDE;
        FL_DEFAULT_COPY_AND_MOVE(ReverseTrapezoid)

        virtual std::string className() const FL_IOVERRIDE;
        virtual std::string parameters() const FL_IOVERRIDE;
        virtual void configure(const std::string& parameters) FL_IOVERRIDE;
        virtual Complexity complexity() const FL_IOVERRIDE;
        virtual scalar membership(scalar x) const FL_IOVERRIDE;
        virtual void setVertexA(scalar a);
        virtual scalar getVertexA() const;
        virtual void setVertexB(scalar b);
        virtual scalar getVertexB() const;
        virtual void setVertexC(scalar c);
        virtual scalar getVertexC() const;
        virtual void setVertexD(scalar d);
        virtual scalar getVertexD() const;
        virtual ReverseTrapezoid* clone() const FL_IOVERRIDE;
        static Term* constructor();

    private:
        scalar _vertexA, _vertexB, _vertexC, _vertexD;
    };
}
#endif // REVERSETRAPEZOID_HPP
